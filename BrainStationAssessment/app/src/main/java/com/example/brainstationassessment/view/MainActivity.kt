package com.example.brainstationassessment.view

import android.os.Bundle
import com.example.brainstationassessment.base.BaseActivity
import com.example.brainstationassessment.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override fun getViewBinding() = ActivityMainBinding.inflate(layoutInflater, null, false)

    override fun init(savedInstanceState: Bundle?) {

    }

}