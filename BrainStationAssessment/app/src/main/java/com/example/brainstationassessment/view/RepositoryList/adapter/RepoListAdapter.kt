package com.example.brainstationassessment.view.RepositoryList.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.brainstationassessment.R
import com.example.brainstationassessment.Utils.DateTimeUtils
import com.example.brainstationassessment.Utils.DiffCallback
import com.example.brainstationassessment.Utils.IntUtils
import com.example.brainstationassessment.databinding.RowRepoListBinding
import com.example.brainstationassessment.view.RepositoryList.model.Item

class RepoListAdapter(
    private var repoList: List<Item>,
    private val context: Context,
    private val glideManager: RequestManager,
    private var listener: OnRepoClickListener
) : RecyclerView.Adapter<RepoListAdapter.ViewHolder>()  {

    inner class ViewHolder(val binding: RowRepoListBinding) :
        RecyclerView.ViewHolder(binding.root)

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(list: List<Item>) {
        val diffCallback = DiffCallback(repoList, list)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        repoList = list
        diffResult.dispatchUpdatesTo(this)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowRepoListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = repoList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo = repoList[position]
        holder.binding.apply {
            glideManager.load(repo.owner.avatarUrl).placeholder(R.drawable.ic_github_icon).into(imgMain)
            txtName.text = repo.fullName
            txtDescription.text = repo.description
            txtLanguageName.text = repo.language ?: "Not Specified"
            txtTotalStar.text = repo.stargazersCount?.let { IntUtils.kFormatter(it) }
            txtUpdatedAt.text = "Updated "+DateTimeUtils.getFeedbackTime(context,repo.updatedAt).toString()

            val topicList = mutableListOf<String>()

            repo.topics.forEach {
                if (it != null) {
                    if (!it.contains("-")){
                        topicList.add(it)
                    }
                }
            }

            rvRepoTopic.apply {
                setHasFixedSize(true)
                adapter = RepoTopicAdapter(topicList)
            }

           root.setOnClickListener {
               listener.onClick(repo)
           }
        }
    }

    interface OnRepoClickListener {
        fun onClick(repo: Item?)

    }
}