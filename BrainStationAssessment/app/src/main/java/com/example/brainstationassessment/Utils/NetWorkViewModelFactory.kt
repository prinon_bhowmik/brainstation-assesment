package com.example.brainstationassessment.Utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.brainstationassessment.base.BaseRepository
import com.example.brainstationassessment.view.RepositoryList.repository.RepoListRepository
import com.example.brainstationassessment.view.RepositoryList.viewmodel.RepoListVM

class NetWorkViewModelFactory(
    private val repository: BaseRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(RepoListVM::class.java) -> RepoListVM(repository as RepoListRepository) as T
            else -> throw IllegalArgumentException("NetworkViewModelFactory Not Found")
        }
    }

}