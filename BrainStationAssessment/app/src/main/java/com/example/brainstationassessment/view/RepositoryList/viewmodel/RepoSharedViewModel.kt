package com.example.brainstationassessment.view.RepositoryList.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.brainstationassessment.view.RepositoryList.model.Item

class RepoSharedViewModel: ViewModel() {
    private val _repoItem = MutableLiveData<Item?>()
    val repoItem : LiveData<Item?> = _repoItem

    fun setItem(item: Item?){
        _repoItem.value = item
    }
}