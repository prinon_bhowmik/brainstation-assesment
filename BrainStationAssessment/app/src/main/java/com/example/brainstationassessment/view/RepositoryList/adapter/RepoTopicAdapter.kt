package com.example.brainstationassessment.view.RepositoryList.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.brainstationassessment.databinding.RowRepoTopicListBinding

class RepoTopicAdapter(
    private var topicList: List<String>,
) : RecyclerView.Adapter<RepoTopicAdapter.ViewHolder>()   {

    inner class ViewHolder(val binding: RowRepoTopicListBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoTopicAdapter.ViewHolder {
        val binding = RowRepoTopicListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (topicList.size <6){
            topicList.size
        }else{
            6
        }
    }

    override fun onBindViewHolder(holder: RepoTopicAdapter.ViewHolder, position: Int) {
        holder.binding.txtTopicName.text = topicList[position]
    }
}