package com.example.brainstationassessment.base

object AppConstants {
    // JSON STATUS =====================================================================================
    const val JSON_VALID_KEY = "application/json"
    const val CONTENT_TYPE_KEY = "content-type"
}