package com.example.brainstationassessment.view.RepositoryList.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.brainstationassessment.Utils.DataState
import com.example.brainstationassessment.view.RepositoryList.repository.RepoListRepository
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Response

class RepoListVM(private val repository: RepoListRepository) : ViewModel() {

    private val _getRepoListResponse : MutableLiveData<DataState<Response<ResponseBody>>> = MutableLiveData()
    val getRepoListResponse : LiveData<DataState<Response<ResponseBody>>> get() = _getRepoListResponse

    fun getRepoList(
        typeToken: String,
        query: String,
        sort: String,
        page: Int,
        perPage: Int,
    ) = viewModelScope.launch {
        _getRepoListResponse.value = DataState.Loading
        _getRepoListResponse.value = repository.repoListData(typeToken,query, sort, page, perPage)
    }
}