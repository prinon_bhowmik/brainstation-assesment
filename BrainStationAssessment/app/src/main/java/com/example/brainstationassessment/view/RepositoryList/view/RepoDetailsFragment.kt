package com.example.brainstationassessment.view.RepositoryList.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.example.brainstationassessment.R
import com.example.brainstationassessment.Utils.BundleUtils
import com.example.brainstationassessment.Utils.DateTimeUtils
import com.example.brainstationassessment.Utils.IntUtils
import com.example.brainstationassessment.Utils.TextUtils
import com.example.brainstationassessment.base.AppConstants
import com.example.brainstationassessment.base.BaseFragment
import com.example.brainstationassessment.databinding.FragmentRepoDetailsBinding
import com.example.brainstationassessment.view.RepositoryList.model.Item
import com.example.brainstationassessment.view.RepositoryList.network.ApiRepoList
import com.example.brainstationassessment.view.RepositoryList.repository.RepoListRepository
import com.example.brainstationassessment.view.RepositoryList.viewmodel.RepoListVM
import com.example.brainstationassessment.view.RepositoryList.viewmodel.RepoSharedViewModel

class RepoDetailsFragment : BaseFragment<RepoListVM,FragmentRepoDetailsBinding,RepoListRepository>() {
    override fun getViewModel() = RepoListVM::class.java
    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentRepoDetailsBinding.inflate(inflater, container, false)

    override fun getRepository() = RepoListRepository(remoteDataSource.buildApi(ApiRepoList::class.java,mContext))

    private val sharedViewModel : RepoSharedViewModel by activityViewModels()

    override fun init(savedInstanceState: Bundle?) {
        checkArguments()

    }

    private fun checkArguments() {
        sharedViewModel.repoItem.observe(viewLifecycleOwner){
            if (it != null){
                setData(it)
            }
        }


    }

    private fun setData(repo: Item?) = with(binding) {
        repo?.let { item ->
            glideManager.load(item.owner.avatarUrl).placeholder(R.drawable.ic_github_icon).into(imgMain)
            txtName.text = item.name?.replaceFirstChar { f-> f.uppercase() }
            txtStar.text = item.visibility?.replaceFirstChar {f -> f.uppercase()}
            txtDescription.text = item.description
            txtLink.text = item.homepage?.replace("https://","")
            txtLicense.text = item.license.key
            txtTotalStar.text = item.stargazersCount?.let { it1 -> IntUtils.kFormatter(it1) }
            txtTotalFolk.text = item.forksCount?.let { it1 -> IntUtils.kFormatter(it1) }
            txtLastUpdate.text =
                DateTimeUtils.uTCTimeToLocalTime(mContext,item.updatedAt.toString())?.let {
                    TextUtils.changeTextLastColor("Last Update: ", it,
                        ContextCompat.getColor(mContext,R.color.blue))
                }


        }

    }


}