package com.example.brainstationassessment.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.brainstationassessment.Utils.LoadingDialog
import com.example.brainstationassessment.Utils.NetWorkViewModelFactory
import com.example.brainstationassessment.network.RemoteDataSource

abstract class BaseFragment<VM : ViewModel, B : ViewBinding, R : BaseRepository> : Fragment() {

    protected val TAG by lazy {
        "#X_${parentFragmentManager.fragments.last()::class.simpleName}"
    }
    protected val viewModel by lazy {
        ViewModelProvider(viewModelStore, NetWorkViewModelFactory(getRepository()))[getViewModel()]
    }

    protected val glideManager by lazy {
        Glide.with(this).applyDefaultRequestOptions(RequestOptions().diskCacheStrategy(
            DiskCacheStrategy.ALL))
    }

    private var _binding: B? = null
    protected val binding get() = _binding!!
    protected val remoteDataSource = RemoteDataSource()
    protected lateinit var mContext: Context
    protected lateinit var loadingDialog: LoadingDialog
    private lateinit var toast: Toast
    protected abstract fun getViewModel(): Class<VM>
    protected abstract fun getBinding(inflater: LayoutInflater, container: ViewGroup?): B
    protected abstract fun getRepository(): R
    protected abstract fun init(savedInstanceState: Bundle?)

    override fun onAttach(context: Context) {
        mContext = context
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = getBinding(inflater, container)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadingDialog = LoadingDialog(mContext)
        toast = Toast.makeText(mContext, null, Toast.LENGTH_SHORT)
        init(savedInstanceState)
    }

    protected fun showToast(text: String?) {
        if (!text.isNullOrBlank()) {
            toast.apply {
                setText(text)
                show()
            }
        }
    }

    override fun onDetach() {
        _binding = null
        super.onDetach()
    }

    protected fun goBack() {
        if (parentFragmentManager.backStackEntryCount == 0) activity?.finish()
        else findNavController().popBackStack()
    }
}