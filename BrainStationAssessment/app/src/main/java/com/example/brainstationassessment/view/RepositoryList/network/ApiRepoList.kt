package com.example.brainstationassessment.view.RepositoryList.network

import com.example.brainstationassessment.view.RepositoryList.others.RepoConstants
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiRepoList {

    @GET(RepoConstants.ENDPOINT)
    suspend fun getRepoList(
        @Header("Authorization") typeToken: String,
        @Query("q") query: String,
        @Query("sort") sort: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int,
    ): Response<ResponseBody>
}