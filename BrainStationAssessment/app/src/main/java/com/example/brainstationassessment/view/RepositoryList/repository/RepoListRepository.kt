package com.example.brainstationassessment.view.RepositoryList.repository

import com.example.brainstationassessment.base.BaseRepository
import com.example.brainstationassessment.view.RepositoryList.network.ApiRepoList

class RepoListRepository(private val api: ApiRepoList) : BaseRepository() {
    suspend fun repoListData(
        typeToken: String,
        query: String,
        sort: String,
        page: Int,
        perPage: Int,
    ) = safeApiCall { api.getRepoList(typeToken,query, sort,page, perPage) }
}