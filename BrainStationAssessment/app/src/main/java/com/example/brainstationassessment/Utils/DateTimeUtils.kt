package com.example.brainstationassessment.Utils

import android.annotation.SuppressLint
import android.content.Context
import com.example.brainstationassessment.R
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import kotlin.time.DurationUnit
import kotlin.time.toTimeUnit

object DateTimeUtils {
    @SuppressLint("SimpleDateFormat")
    fun uTCTimeToLocalTime(context: Context,utcTime: String): String? {
        if (utcTime.isNullOrBlank()) return null
        val inputDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        inputDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val date = inputDateFormat.parse(utcTime)

        if (date != null) {
            val outputDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            outputDateFormat.timeZone = TimeZone.getDefault() // BST timezone

            val formattedTimeWithoutDate = SimpleDateFormat("hh:mm a").format(date)
            val formattedTimeWithDate = SimpleDateFormat("MMM dd, yyyy").format(date)
            val updatedTime = "$formattedTimeWithDate $formattedTimeWithoutDate"

          return updatedTime
        }
        return null
    }

    fun getFeedbackTime(context: Context, dateTime: String?): String? {
        return if (dateTime.isNullOrBlank()) null
        else {
            val calendar = Calendar.getInstance()
            val currentTime = calendar.timeInMillis
            val feedbackTime = calendar.apply {
                time = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(dateTime)
            }.timeInMillis

            val difference = currentTime - feedbackTime
            val days = DurationUnit.MILLISECONDS.toTimeUnit().toDays(difference)
            val months = days / 30
            val years = months / 12
            val hours = DurationUnit.MILLISECONDS.toTimeUnit().toHours(difference)
            val minute = DurationUnit.MILLISECONDS.toTimeUnit().toMinutes(difference)
            val second = DurationUnit.MILLISECONDS.toTimeUnit().toSeconds(difference)

            if (years > 0) context.getString(R.string.any_year_ago, years.toInt())
            else if (months > 0) context.getString(R.string.any_month_ago, months.toInt())
            else if (days > 0) context.getString(R.string.any_day_ago, days)
            else if (hours > 0) context.getString(R.string.any_hour_ago, hours)
            else if (minute > 0) context.getString(R.string.any_min_ago, minute)
            else if (second < 5) context.getString(R.string.just_now)
            else context.getString(R.string.any_seconds_ago, second)
        }
    }

}