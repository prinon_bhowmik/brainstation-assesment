package com.example.brainstationassessment.Utils

object IntUtils {
    fun kFormatter(num: Int): String {
        return if (Math.abs(num) > 999) {
            "${Math.signum(num.toDouble()) * ((Math.abs(num) / 1000).toFloat())}k"
        } else {
            "${Math.signum(num.toDouble()) * Math.abs(num)}"
        }
    }
}