package com.example.brainstationassessment.Utils

import android.os.Build
import android.os.Bundle
import java.io.Serializable

object BundleUtils {
    fun <T : Serializable?> getSerializable(bundle: Bundle?, key: String, clazz: Class<T>): T? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) bundle?.getSerializable(key, clazz)
        else bundle?.getSerializable(key) as T
    }
}