package com.example.brainstationassessment.Utils

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan

object TextUtils {
    fun changeTextLastColor(
        textStart: String, textColor: String, color: Int
    ): SpannableStringBuilder {
        val normalColor = ForegroundColorSpan(color)
        return SpannableStringBuilder(textStart + textColor).apply {
            setSpan(
                normalColor,
                textStart.length,
                textStart.length + textColor.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}