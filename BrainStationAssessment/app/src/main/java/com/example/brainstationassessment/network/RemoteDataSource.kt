package com.example.brainstationassessment.network

import android.content.Context
import com.google.gson.GsonBuilder
import okhttp3.Authenticator
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RemoteDataSource {
    private val converterFactory = GsonConverterFactory.create(GsonBuilder().setLenient().create())

    companion object {
        @JvmStatic
        val apiUrl = "https://api.github.com/"
    }

    fun <Api> buildApi(
        api: Class<Api>,
        context: Context
    ): Api {
        return Retrofit.Builder()
            .baseUrl(apiUrl)
            .client(getClient())
            .addConverterFactory(converterFactory)
            .build()
            .create(api)
    }

    private fun getClient(
        authenticator: Authenticator? = null,
        offlineInterceptor: Interceptor? = null,
        onlineInterceptor: Interceptor? = null,
        cache: Cache? = null,
    ): OkHttpClient {
        return OkHttpClient.Builder().also { client ->
            client.addInterceptor { chain ->
                chain.proceed(chain.request().newBuilder().also {
                    it.addHeader("Accept", "application/json")
                    it.addHeader("x-app-name", "brain_station")
                }.build())
            }.also { builder ->
                authenticator?.let { builder.authenticator(it) }
                builder.addInterceptor(
                    HttpLoggingInterceptor().also {
                        it.level = HttpLoggingInterceptor.Level.BODY
                    }
                )
                offlineInterceptor?.let { builder.addInterceptor(it) }
                onlineInterceptor?.let { builder.addNetworkInterceptor(it) }
                cache?.let { builder.cache(it) }
            }.build()
        }.build()
    }
}