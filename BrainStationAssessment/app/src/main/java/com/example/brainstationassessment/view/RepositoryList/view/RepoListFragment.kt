package com.example.brainstationassessment.view.RepositoryList.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.brainstationassessment.R
import com.example.brainstationassessment.Utils.DataState
import com.example.brainstationassessment.Utils.NetworkUtils
import com.example.brainstationassessment.base.AppConstants
import com.example.brainstationassessment.base.BaseFragment
import com.example.brainstationassessment.databinding.FragmentRepoListBinding
import com.example.brainstationassessment.view.RepositoryList.adapter.RepoListAdapter
import com.example.brainstationassessment.view.RepositoryList.model.Item
import com.example.brainstationassessment.view.RepositoryList.model.RepoListModel
import com.example.brainstationassessment.view.RepositoryList.network.ApiRepoList
import com.example.brainstationassessment.view.RepositoryList.repository.RepoListRepository
import com.example.brainstationassessment.view.RepositoryList.viewmodel.RepoListVM
import com.example.brainstationassessment.view.RepositoryList.viewmodel.RepoSharedViewModel
import com.google.gson.Gson

class RepoListFragment : BaseFragment<RepoListVM, FragmentRepoListBinding, RepoListRepository>() {
    override fun getViewModel() = RepoListVM::class.java

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentRepoListBinding.inflate(inflater, container, false)

    override fun getRepository() = RepoListRepository(remoteDataSource.buildApi(ApiRepoList::class.java,mContext))

    private var nextPage: Boolean = true
    private var page = 1
    private var perPage = 10
    private var repoList = mutableListOf<Item>()
    private lateinit var repoAdapter: RepoListAdapter
    private val sharedViewModel : RepoSharedViewModel by activityViewModels()

    override fun init(savedInstanceState: Bundle?) {
        observeRepoListResponse()

        activity?.packageName?.let { viewModel.getRepoList(it,"android","stars", page, perPage) }

        setRepoAdapter()
    }

    private fun setRepoAdapter() = with(binding) {
        repoList = mutableListOf()

        repoAdapter = RepoListAdapter(repoList,mContext,glideManager, object : RepoListAdapter.OnRepoClickListener{
            override fun onClick(repo: Item?) {
                sharedViewModel.setItem(repo)
                findNavController().navigate(
                    R.id.action_repoListFragment_to_repoDetailsFragment
                )

            }
        })
        rvRepoTopic.apply {
            setHasFixedSize(true)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE && (recyclerView.layoutManager as
                                LinearLayoutManager).findLastVisibleItemPosition() >= repoList.lastIndex
                    ) {
                        if (nextPage) {
                            page = ++page
                            activity?.packageName?.let { viewModel.getRepoList(it,"android","stars", page, perPage) }
                        }
                    }
                }
            })
            adapter = repoAdapter
        }
    }

    private fun observeRepoListResponse() {
        viewModel.getRepoListResponse.observe(viewLifecycleOwner){
            when(it){

                is DataState.Success -> {
                    loadingDialog.dismiss()

                    val body = it.value.body()?.string()
                    if (NetworkUtils.isValidResponse(it)) {
                        val response = Gson().fromJson(body, RepoListModel::class.java)
                        if (response != null){
                            setRepoData(response)
                        }
                    } else showToast(getString(R.string.something_went_wrong))
                }

                is DataState.Loading -> {
                    loadingDialog.show()
                }

                is DataState.Error -> {
                    loadingDialog.dismiss()
                    if (it.isNetworkError) showToast(getString(R.string.internet_conn_lost_title))

                }
            }
        }
    }

    private fun setRepoData(response: RepoListModel) {
        if (response.message == null || response.items?.isNotEmpty() == true){
            response.items?.let { repoList.addAll(it) }
            repoAdapter.updateList(repoList)
        }else {
            nextPage = false
        }

    }

}